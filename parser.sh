#!/bin/bash
rankname=("E" "D" "C" "B" "BB" "BB+" "A" "AA" "AA+" "S" "SS" "SS+")
ranksta=(0 200000 300000 350000 400000 450000 500000 550000 600000 700000 800000 900000)
rankexp=(0 250000 350000 400000 500000 550000 600000 650000 700000 800000 900000 950000)
rankult=(0 300000 400000 500000 600000 650000 700000 750000 800000 900000 950000 975000)
rankman=(0 400000 500000 600000 700000 750000 800000 850000 900000 950000 965000 980000)
rankcon=(0 300000 400000 500000 600000 650000 700000 750000 800000 850000 900000 950000)

btv () {
  [[ ${1} == "true" ]] && echo 0 || echo 1;
}

sctorank () {
  case $2 in
    sta)
      declare -n rank=ranksta;
      ;;
    exp)
      declare -n rank=rankexp;
      ;;
    ult)
      declare -n rank=rankult;
      ;;
    man)
      declare -n rank=rankman;
      ;;
    con)
      declare -n rank=rankcon;
      ;;
  esac
  rn=""
  for (( i=${#rankname[@]}-1; i >= 0; i-- ))
  do
    if (( $1 >= ${rank[i]} )); then
      rn=${rankname[i]};
      break
    fi
  done
  echo $rn;
}

calcspec () {
  g=$(echo $1 | jq '.gr');
  o=$(echo $1 | jq '.ok');
  m=$(echo $1 | jq '.ms');
  showspec=$(btv $(echo ${1} |  jq '.spec'));
  [[ $showspec == 0 ]] && echo "${g}-${o}-${m}" || echo "";
}

calcscore () {
  n=$1
  s=$(echo $2 | jq '.score');
  g=$(echo $2 | jq '.gr');
  o=$(echo $2 | jq '.ok');
  m=$(echo $2 | jq '.ms');
  (( ${n} == 0 )) && n=999;
  p=$(calc -d "${n} - (${g} + ${o} + ${m})");
  v=$(calc -d "1000000 / ${n}");
  (( ${s} != 0 )) && echo $s || echo $(calc -d "ceil((${v} * ${p}) + (0.7 * ${v} * ${g}) + (0.5 * ${v} * ${o}) + (0 * ${v} * ${m}))");
}

isap () {
  isapisplayed=$(btv $(echo ${1} | jq '.played'));
  isapspec=$(($(echo ${1} | jq '.gr') + $(echo ${1} | jq '.ok') + $(echo ${1} | jq '.ms')));
  [[ $isapisplayed == 0 ]] && [[ $isapspec == 0 ]] && echo "ap" || echo "";
}

isplayed () {
  [[ $(btv ${1}) == 0 ]] && echo "" || echo "np";
}

isreleased () {
  (($(date --date="${1}" "+%s") < $(date "+%s"))) && echo "" || echo "nr";
}

# to future tsoobs: I AM SO SORRY. love, past tsoobs

calculate () {
  meta=$(cat music/${1}/meta.json | jq '.')
  data=$(cat music/${1}/data.json | jq '.')
  ttl=$(echo $meta | jq -r '.title')
  art=$(echo $meta | jq -r '.artist')
  gen=$(echo $meta | jq -r '.genre')
  srl=$(isreleased $(echo $meta | jq -r '.difficulty.sta.release'));
  erl=$(isreleased $(echo $meta | jq -r '.difficulty.exp.release'));
  url=$(isreleased $(echo $meta | jq -r '.difficulty.ult.release'));
  mrl=$(isreleased $(echo $meta | jq -r '.difficulty.man.release'));
  crl=$(isreleased $(echo $meta | jq -r '.difficulty.con.release'));
  stn=$(echo $meta | jq -r '.difficulty.sta.notes');
  etn=$(echo $meta | jq -r '.difficulty.exp.notes');
  utn=$(echo $meta | jq -r '.difficulty.ult.notes');
  mtn=$(echo $meta | jq -r '.difficulty.man.notes');
  ctn=$(echo $meta | jq -r '.difficulty.con.notes');
  slv=$(echo $meta | jq -r '.difficulty.sta.lv');
  elv=$(echo $meta | jq -r '.difficulty.exp.lv');
  ulv=$(echo $meta | jq -r '.difficulty.ult.lv');
  mlv=$(echo $meta | jq -r '.difficulty.man.lv');
  clv=$(echo $meta | jq -r '.difficulty.con.lv');
  spl=$(isplayed $(echo $data | jq -r '.difficulty.sta.played'));
  epl=$(isplayed $(echo $data | jq -r '.difficulty.exp.played'));
  upl=$(isplayed $(echo $data | jq -r '.difficulty.ult.played'));
  mpl=$(isplayed $(echo $data | jq -r '.difficulty.man.played'));
  cpl=$(isplayed $(echo $data | jq -r '.difficulty.con.played'));
  sap=$(isap $(echo $data | jq -c '.difficulty.sta'));
  eap=$(isap $(echo $data | jq -c '.difficulty.exp'));
  uap=$(isap $(echo $data | jq -c '.difficulty.ult'));
  map=$(isap $(echo $data | jq -c '.difficulty.man'));
  cap=$(isap $(echo $data | jq -c '.difficulty.con'));
  ssc=$(calcscore ${stn} $(echo $data | jq -c '.difficulty.sta'));
  esc=$(calcscore ${etn} $(echo $data | jq -c '.difficulty.exp'));
  usc=$(calcscore ${utn} $(echo $data | jq -c '.difficulty.ult'));
  msc=$(calcscore ${mtn} $(echo $data | jq -c '.difficulty.man'));
  csc=$(calcscore ${ctn} $(echo $data | jq -c '.difficulty.con'));
  srn=$(sctorank ${ssc} sta);
  ern=$(sctorank ${esc} exp);
  urn=$(sctorank ${usc} ult);
  mrn=$(sctorank ${msc} man);
  crn=$(sctorank ${csc} con);
  ssp=$(calcspec $(echo $data | jq -c '.difficulty.sta'));
  esp=$(calcspec $(echo $data | jq -c '.difficulty.exp'));
  usp=$(calcspec $(echo $data | jq -c '.difficulty.ult'));
  msp=$(calcspec $(echo $data | jq -c '.difficulty.man'));
  csp=$(calcspec $(echo $data | jq -c '.difficulty.con'));
  [[ $sap == "ap" ]] && ssp="ALL PERFECT";
  [[ $eap == "ap" ]] && esp="ALL PERFECT";
  [[ $uap == "ap" ]] && usp="ALL PERFECT";
  [[ $map == "ap" ]] && msp="ALL PERFECT";
  [[ $cap == "ap" ]] && csp="ALL PERFECT";
  [[ $spl == "np" ]] && ssc="-";
  [[ $epl == "np" ]] && esc="-";
  [[ $upl == "np" ]] && usc="-";
  [[ $mpl == "np" ]] && msc="-";
  [[ $cpl == "np" ]] && csc="-";
  [[ $srl == "nr" ]] && ssc="coming soon";
  [[ $erl == "nr" ]] && esc="coming soon";
  [[ $url == "nr" ]] && usc="coming soon";
  [[ $mrl == "nr" ]] && msc="coming soon";
  [[ $crl == "nr" ]] && csc="coming soon";
  echo $(cat table_template.html |
    sed -e "s/%MUS_ID%/${1}/g" |
    sed -e "s/%ULT_AP%/${uap}/g" |
    sed -e "s/%MUS_TITLE%/${ttl}/g" |
    sed -e "s/%MUS_ARTIST%/${art}/g" |
    sed -e "s/%MUS_GENRE%/${gen}/g" |
    sed -e "s/%STA_RL%/${srl}/g" |
    sed -e "s/%STA_LV%/${slv}/g" |
    sed -e "s/%STA_NP%/${spl}/g" |
    sed -e "s/%STA_AP%/${sap}/g" |
    sed -e "s/%STA_SCORE%/${ssc}/g" |
    sed -e "s/%STA_GRADE%/${srn}/g" |
    sed -e "s/%STA_SPEC%/${ssp}/g" |
    sed -e "s/%EXP_RL%/${erl}/g" |
    sed -e "s/%EXP_LV%/${elv}/g" |
    sed -e "s/%EXP_NP%/${epl}/g" |
    sed -e "s/%EXP_AP%/${eap}/g" |
    sed -e "s/%EXP_SCORE%/${esc}/g" |
    sed -e "s/%EXP_GRADE%/${ern}/g" |
    sed -e "s/%EXP_SPEC%/${esp}/g" |
    sed -e "s/%ULT_RL%/${url}/g" |
    sed -e "s/%ULT_LV%/${ulv}/g" |
    sed -e "s/%ULT_NP%/${upl}/g" |
    sed -e "s/%ULT_AP%/${uap}/g" |
    sed -e "s/%ULT_SCORE%/${usc}/g" |
    sed -e "s/%ULT_GRADE%/${urn}/g" |
    sed -e "s/%ULT_SPEC%/${usp}/g" |
    sed -e "s/%MAN_RL%/${mrl}/g" |
    sed -e "s/%MAN_LV%/${mlv}/g" |
    sed -e "s/%MAN_NP%/${mpl}/g" |
    sed -e "s/%MAN_AP%/${map}/g" |
    sed -e "s/%MAN_SCORE%/${msc}/g" |
    sed -e "s/%MAN_GRADE%/${mrn}/g" |
    sed -e "s/%MAN_SPEC%/${msp}/g" |
    sed -e "s/%CON_RL%/${crl}/g" |
    sed -e "s/%CON_LV%/${clv}/g" |
    sed -e "s/%CON_NP%/${cpl}/g" |
    sed -e "s/%CON_AP%/${cap}/g" |
    sed -e "s/%CON_SCORE%/${csc}/g" |
    sed -e "s/%CON_GRADE%/${crn}/g" |
    sed -e "s/%CON_SPEC%/${csp}/g" |
    sed -e "s/DOUBLESLASH/\/\//g"
  );
  return 0;
}

cp index-template.html index.html;
for i in music/*; do
  j=$(basename ${i});
  calculate $j >> index.html;
  echo "${j} completed";
done
