while true; do
  echo "楽曲の成績ファイルはリセットして、保存したデータは消してしまいます。・all song stat files will be reset, ALL SAVED DATA WILL BE LOST."
  read -p "よろしいですか？ Are you sure?［Y | N］" yn
  case $yn in
    [Yy]* ) for i in music/*; do cp data_template.json $i/data.json; done; break;;
    * ) exit;;
  esac
done
