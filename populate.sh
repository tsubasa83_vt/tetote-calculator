#!/bin/bash
#
#

# does the cache directory exist? if not make it
mkdir -p .cache

# start by downloading the music page into cache
curl https://tetoteconnect.jp/music.html > .cache/music.html
# get rid of the "NEW" marker element in new songs. this actually gets in the way of scraping later
sed -i '/newmark\.png/d' .cache/music.html

# STRUCTURE OF MUSIC DEPICTIONS:
#             <figure class="hover-parent">
#                <img src="images/music_jacket_[X].jpg"/>
#                <figcaption class="hover-mask">
#                    [Y]／[Z]
#                </figcaption>
#                <p class="genre [W]"></p>
#            </figure>
#
# [X] is the music ID (primary key)
# [Y] is the name of the song
# [Z] is the artist
# [W] is the genre name (original, virtual, pops, toho, game, variety)
# NOTICE: some songs will be lumped into multiple categories. the genre listed here is to be treated as its primary

# md: line numbers for each music depiction.
# - input cached music page
# - then we find each hover parent
# - then print each one, INCLUDING THE NEXT FIVE LINES, to an "isolated" file. (cut the leading whitespace)
cat .cache/music.html | grep "figure class=\"hover-parent\"" -A 5 | sed -e 's/^[ \t]*//' > .cache/music-isolated.html

# transform the depictions into iterable lists. X for id, W for genre, and V for song/artist

# list X is every 2nd line, music id starts at byte 31 and ends on a period:
cat .cache/music-isolated.html | sed -n '2~7p' | cut -b 31- | cut -d "." -f 1 > .cache/x
# list W is every 6th lines, genre starts at byte 17 and ends on a quote:
cat .cache/music-isolated.html | sed -n '6~7p' | cut -b 17- | cut -d\" -f 1 > .cache/w
# list V is every 4th line. slashes delimit song and artist, standardize to caret:
cat .cache/music-isolated.html | sed -n '4~7p' | sed 's/／/^/g' | sed 's/\/\ /^/g' > .cache/v

# get line count of each cache file. THEY SHOULD BE EQUAL OR SOMETHING HAS GONE REALLY WRONG
vl=$(cat .cache/v | wc -l);
wl=$(cat .cache/w | wc -l);
xl=$(cat .cache/x | wc -l);

# check if something has gone really wrong
if [ "$vl" != "$wl" ] || [ "$vl" != "$xl" ] || [ "$wl" != "$xl" ]; then
  echo "v, w, x are not equal: irregularity?";
  exit 14;
fi

# time to get the goods. since they're equal, iterate through a for loop and get line p from each cache
for ((p = 1; p < $wl + 1; p++)); do
  v=$(sed -n "${p}p" .cache/v)
  w=$(sed -n "${p}p" .cache/w)
  x=$(sed -n "${p}p" .cache/x)
  # turn v into y and z using the delimiter, making sure to remove any leading/trailing whitespace
  y=$(echo $v | cut -d '^' -f 1 | sed -e 's/^[ \t]*//;s/[ \t]*$//' | tr -d '\r');
  z=$(echo $v | cut -d '^' -f 2 | sed -e 's/^[ \t]*//;s/[ \t]*$//' | tr -d '\r');
  # print what we have before doing anything else
  echo "$x^$w^$y^$z";
  # get the jacket if it doesn't already exist
  mkdir -p music/$x;
  if [[ ! -f music/$x/jacket.jpg ]]; then
    curl https://tetoteconnect.jp/images/music_jacket_$x.jpg > music/$x/jacket.jpg
  fi
  # make a meta template if it doesn't already exist
  if [[ ! -f music/$x/meta.json ]]; then
    sed "s/AAAA/$x/;s/BBBB/$y/;s/CCCC/$z/;s/DDDD/$w/" meta_template.json > music/$x/meta.json
  fi
  if [[ ! -f music/$x/data.json ]]; then
    cp data_template.json music/$x/data.json
  fi
done
